import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

class GroupTest {


    private static final String name = "Kostiantyn";
    private static final String surName = "Honcharenko";
    private static final int index = 90445;


    @Test
    void addStudent() {
        Student defStudent = new Student(index, name, surName);
        Group group = new Group();
        group.addStudent(defStudent);
        assertEquals(defStudent, group.findByIndex(defStudent.getIndex()));
    }

    @Test
    void addSameStudent() {
        Student defStudent = new Student(index, name, surName);
        Group group = new Group();
        group.addStudent(defStudent);
        group.addStudent(defStudent);
        assertEquals(1, group.getStudents().size());
    }

    @Test
    void deleteStudent() {
        Student defStudent = new Student(index, name, surName);
        Group group = new Group();
        group.addStudent(defStudent);
        group.deleteStudent(new Student(index, name, surName));
        assertEquals(0, group.getStudents().size());
    }

    @Test
    void editStudent() {
        Student defStudent = new Student(index, name, surName);
        Group group = new Group();
        group.addStudent(defStudent);
        group.editStudent(defStudent.getIndex(), "Kostia", "Honch");
        assertEquals("Kostia", group.findByIndex(defStudent.getIndex()).getName());
    }

    @Test
    void findByIndex() {
        Student defStudent = new Student(index, name, surName);
        Group group = new Group();
        group.addStudent(defStudent);
        Student newS = group.findByIndex(defStudent.getIndex());
        assertEquals(newS, defStudent);
    }

    @Test
    void addMeeting() {
        Group group = new Group();
        String date = "03.12.2018";
        Map<Integer, Boolean> presence = new HashMap<>();
        presence.put(90226, true);
        group.addMeeting(date, presence);
        assertEquals("03.12.2018", group.getMeetings().get(0).getDate());
    }

    @Test
    void verifyPresense() {
        Group group = new Group();
        String date = "03.12.2018";
        Map<Integer, Boolean> presence = new HashMap<>();
        presence.put(90226, true);
        group.addMeeting(date, presence);
        assertTrue(group.verifyPresense(date, 90226));
    }

    @Test
    void editPresence() {
        Group group = new Group();
        String date = "03.12.2018";
        Map<Integer, Boolean> presence = new HashMap<>();
        presence.put(90226, true);
        group.addMeeting(date, presence);
        group.editPresence("03.12.2018", 90226, false);
        assertTrue(!group.verifyPresense(date, 90226));
    }

    @Test
    void addMark() {
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(4);
        defStudent.addMark(3);
        assertEquals(2, defStudent.getMarks().size());
    }

    @Test
    void addIncorrectMark() {
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(6);
        defStudent.addMark(0);
        assertEquals(0, defStudent.getMarks().size());
    }

    @Test
    void editMark() {
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(4);
        defStudent.addMark(3);
        defStudent.editMark(0, 2);
        defStudent.editMark(1, 2);
        assertTrue(2 == defStudent.getMarks().get(0) && 2 == defStudent.getMarks().get(1));
    }

    @Test
    void deleteMark() {
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(4);
        defStudent.addMark(3);
        defStudent.deleteMark(1);
        defStudent.deleteMark(0);
        assertEquals(0, defStudent.getMarks().size());
    }

    @Test
    void averageMark() {
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(4);
        defStudent.addMark(3);
        defStudent.calcAverageMark();
        assertEquals(3.5, defStudent.getAverageMark());
    }

    @Test
    void averageGroup() {
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(3);
        defStudent.addMark(3);
        defStudent.calcAverageMark();
        Student defStudent2 = new Student(index + 1, "Ivan", surName);
        defStudent2.addMark(5);
        defStudent2.addMark(5);
        defStudent2.calcAverageMark();
        Group group = new Group();
        group.addStudent(defStudent);
        group.addStudent(defStudent2);
        assertEquals(4, group.calcAverage());
    }

    @Test
    void countNoPresence() {
        Group group = new Group();

        Map<Integer, Boolean> presence = new HashMap<>();
        presence.put(90226, true);
        presence.put(90227, true);
        presence.put(90228, false);
        group.addMeeting("03.12.2018", presence);

        Map<Integer, Boolean> presence2 = new HashMap<>();
        presence2.put(90226, true);
        presence2.put(90227, true);
        presence2.put(90228, false);
        group.addMeeting("04.12.2018", presence2);

        Map<Integer, Boolean> presence3 = new HashMap<>();
        presence3.put(90226, true);
        presence3.put(90227, true);
        presence3.put(90228, true);
        group.addMeeting("05.12.2018", presence3);

        assertEquals(2, group.countNoPresence(90228));
    }

    @Test
    void writeInfo() throws IOException {
        Group group = new Group();
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(4);
        defStudent.addMark(3);
        defStudent.calcAverageMark();
        group.addStudent(defStudent);
        Map<Integer, Boolean> presence = new HashMap<>();
        presence.put(90226, true);
        group.addMeeting("03.12.2018", presence);

        String text = group.buildString();
        group.writeInfo();
        String fileText = readFile();

        assertEquals(text, fileText);
    }

    @Test
    void readInfo() throws IOException {
        Group group = new Group();
        Student defStudent = new Student(index, name, surName);
        defStudent.addMark(4);
        defStudent.addMark(3);
        defStudent.calcAverageMark();
        group.addStudent(defStudent);

        Map<Integer, Boolean> presence = new HashMap<>();
        presence.put(90221, true);
        presence.put(90222, true);
        presence.put(90223, false);
        group.addMeeting("03.12.2018", presence);

        Map<Integer, Boolean> presence2 = new HashMap<>();
        presence2.put(90221, false);
        presence2.put(90222, true);
        presence2.put(90223, false);
        group.addMeeting("04.12.2018", presence2);
        String text = group.buildString();
        group.writeInfo();

        Group groupFromFile = new Group();
        groupFromFile.readInfo();
        String infoFromFile = groupFromFile.buildString();

        assertEquals(text, infoFromFile);
    }

    private String readFile() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("groupInfo.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            sb.delete(sb.length() - 2, sb.length());
            return sb.toString();
        }
    }

}