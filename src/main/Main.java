import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

public class Main {

    public static void main (String args[]) {
//        Group gr = new Group();
//        gr.addStudent(new Student(90226, "Kostia", "Honcharenko"));
//        gr.addStudent(new Student(90227, "Vadym", "Honcharenko"));
//        gr.addStudent(new Student(90227, "Vadym", "Honcharenko"));
//        gr.addStudent(new Student(90228, "Ivan", "Honcharenko"));
//
//        gr.showStudents();
//
//        gr.deleteStudent(new Student(90227, "Vadym", "Honcharenko"));
//
//        gr.showStudents();
//
//        gr.editStudent(90226, "Kostiantyn", "Honcharenko");
//
//        gr.showStudents();
//
//        gr.addMeeting("03.12.2018", new HashMap() {{
//            put(90226, true);
//            put(90227, true);
//            put(90228, false);
//        }});
//        gr.addMeeting("04.12.2018", new HashMap() {{
//            put(90226, true);
//            put(90227, true);
//            put(90228, false);
//        }});
//        gr.addMeeting("05.12.2018", new HashMap() {{
//            put(90226, true);
//            put(90227, true);
//            put(90228, true);
//        }});
//
//        gr.showMeetings();
//        System.out.println(gr.verifyPresense("04.12.2018", 90226));
//        gr.editPresence("04.12.2018", 90226, false);
//        System.out.println(gr.verifyPresense("04.12.2018", 90226));
//
//        Student stud = gr.findByIndex(90226);
//        stud.addMark(2);
//        stud.addMark(3);
//        stud.addMark(3);
//        stud.addMark(5);
//        stud.calcAverageMark();
//
//        stud = gr.findByIndex(90228);
//        stud.addMark(4);
//        stud.addMark(3);
//        stud.addMark(4);
//        stud.addMark(5);
//        stud.calcAverageMark();
//
//        gr.showStudents();
//
//        stud.editMark(1, 4);
//        stud.editMark(2, 5);
//
//        stud.deleteMark(0);
//        stud.calcAverageMark();
//
//        gr.showStudents();
//
//        System.out.println("main.Group average : " + gr.calcAverage());
//
//        gr.showMeetings();
//        System.out.println("main.Student with index " + 90228 + " was absent : " + gr.countNoPresence(90228));

        Group group = new Group();
        Student defStudent = new Student(90445, "Kostiantyn", "Honcharenko");
        defStudent.addMark(4);
        defStudent.addMark(3);
        defStudent.calcAverageMark();
        group.addStudent(defStudent);
        group.addMeeting("03.12.2018", new HashMap() {{
            put(90221, true);
            put(90222, true);
            put(90223, false);
        }});
        group.addMeeting("04.12.2018", new HashMap() {{
            put(70221, false);
            put(70222, true);
            put(70223, false);
        }});


        try {
            group.writeInfo();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            group.readInfo();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
