import java.io.*;
import java.util.*;

public class Group {
    List<Student> students = new ArrayList<>();
    List<Meeting> meetings = new ArrayList<>();

    void addStudent(Student newStudent) {
        boolean uniq = true;
        int newIndex = newStudent.getIndex();

        for (Student stud : students) {
            if(newIndex == stud.getIndex()) {
                uniq = false;
                break;
            }
        }

        if(uniq)
            students.add(newStudent);
        else
            System.out.println("Already exists");
    }

    void deleteStudent (Student student) {
        int index = student.getIndex();

        students.removeIf(stud -> index == stud.getIndex());
    }

    void editStudent(int index, String newName, String newSurname) {
        for (Student stud : students) {
            if(index == stud.getIndex()) {
                stud.setName(newName);
                stud.setSurname(newSurname);
            }
        }
    }

    Student findByIndex (int index) {
        for (Student stud : students) {
            if(index == stud.getIndex()) {
                return stud;
            }
        }
        return null;
    }

    void showStudents () {
        for(Student stud : students) {
            System.out.println(stud.toString() + " | " + countNoPresence(stud.getIndex()));
        }
        System.out.println("-");
    }

    float calcAverage () {
        float sum = 0;
        for (Student stud : students) {
            sum += stud.getAverageMark();
        }
        return sum/students.size();
    }

    void addMeeting(String date, Map<Integer, Boolean> presence) {
        if (meetings.size() <= 15)
            meetings.add(new Meeting(date, presence));
    }

    boolean verifyPresense (String date, int index) {
        for (Meeting meet : meetings) {
            if (meet.getDate().equals(date)) {
                return meet.getPresence().get(index);
            }

        }
        return false;
    }

    void editPresence (String date, int index, boolean presence) {
        for (Meeting meet : meetings) {
            if (meet.getDate().equals(date)) {
                meet.getPresence().replace(index, presence);
            }

        }
    }

    void showMeetings () {
        for(Meeting meet : meetings) {
            System.out.println(meet.toString());
        }
        System.out.println("-");
    }

    int countNoPresence (int index) {
        int counter = 0;
        for (Meeting meet : meetings) {
            if (!meet.getPresence().get(index))
                counter ++;
        }
        return counter;
    }

    void writeInfo () throws FileNotFoundException {
        String info = buildString();
        PrintWriter out = new PrintWriter("groupInfo.txt");
        out.println(info);
        out.close();
    }

    String buildString () {
        StringBuilder info = new StringBuilder();
        for (Student stud : students)
            info.append(stud.getInfo());
        info.append("$" + System.lineSeparator());
        for (Meeting meet : meetings)
            info.append(meet.toString());
        return info.toString();
    }

    void readInfo () throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader("groupInfo.txt"))) {
            String line = br.readLine();
            boolean studPart = true;
            while (line != null) {
                if (line.contains("$")) {
                    line = br.readLine();
                    studPart = false;
                }
                if(studPart)
                    parseFirstPart(line);
                else
                    parseSecondPart(line);
                line = br.readLine();
                if(line.equals(""))
                    break;
            }
        }
    }

    void parseFirstPart (String line) {
        String delims = "[ ]";
        String[] tokens = line.split(delims);
        Student student = new Student(Integer.parseInt(tokens[0]), tokens[1], tokens[2]);
        for (int i = 4 ; i < tokens.length ; i ++) {
            student.addMark(Integer.parseInt(tokens[i]));
        }
        student.calcAverageMark();
        addStudent(student);
        System.out.println(Arrays.toString(tokens));
    }

    void parseSecondPart (String line) {
        String delims = "[ ]";
        String[] tokens = line.split(delims);
        String date = tokens[0];
        HashMap<Integer, Boolean> presence = new HashMap<>();
        for (int i = 1 ; i < tokens.length ; i += 4 ) {
            presence.put(Integer.parseInt(tokens[i]), Boolean.valueOf(tokens[i+2]));
        }
        addMeeting(date, presence);
        System.out.println(Arrays.toString(tokens));

    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }
}
