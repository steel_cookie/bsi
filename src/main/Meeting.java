import java.util.*;

public class Meeting {
    String date;
    Map<Integer, Boolean> presence;

    public Meeting (String date, Map<Integer, Boolean> presence) {
        this.date = date;
        this.presence = presence;
    }

    @Override
    public String toString () {
        StringBuilder prString = new StringBuilder();
        prString.append(String.valueOf(this.date) + " ");
        for (Map.Entry<Integer, Boolean> pr : presence.entrySet()) {
            prString
                    .append(String.valueOf(pr.getKey()))
                    .append(" - ")
                    .append(String.valueOf(pr.getValue()))
                    .append(" | ");
        }
        prString.append(System.lineSeparator());
        return prString.toString();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<Integer, Boolean> getPresence() {
        return presence;
    }

    public void setPresence(HashMap<Integer, Boolean> presence) {
        this.presence = presence;
    }
}
