import java.util.ArrayList;
import java.util.List;

public class Student {
    private int index;
    private String name;
    private String surname;
    private List<Integer> marks = new ArrayList<>();
    private float averageMark;

    Student (int index, String name, String surname) {
        this.index = index;
        this.name = name;
        this.surname = surname;
    }

    void addMark (int mark) {
        if( mark >= 2 && mark <=5) {
            marks.add(mark);
        }
    }

    void editMark (int index, int mark) {
        if(marks.size() > index && mark >= 2 && mark <=5 ) {
            marks.remove(index);
            marks.add(index, mark);
        }
    }

    void deleteMark (int index) {
        if(marks.size() > index)
        marks.remove(index);
    }

    int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Integer> getMarks() {
        return marks;
    }

    public void setMarks(List<Integer> marks) {
        this.marks = marks;
    }

    void calcAverageMark() {
        float sum = 0;
        for(Integer mark : marks) {
            sum += mark;
        }
        this.averageMark = sum /marks.size();
    }

    float getAverageMark () {
        return  averageMark;
    }

    @Override
    public String toString () {
        StringBuilder studentString = new StringBuilder();
        studentString
                .append(String.valueOf(this.index) + " ")
                .append(this.name + " ")
                .append(this.surname + " | ");
        for (Integer mark : marks) {
            studentString.append(String.valueOf(mark) + " ");
        }
        studentString.append(" | average " + averageMark);
        return studentString.toString();
    }

    String getInfo () {
        StringBuilder studentString = new StringBuilder();
        studentString
                .append(String.valueOf(this.index) + " ")
                .append(this.name + " ")
                .append(this.surname + " | ");
        for (Integer mark : marks) {
            studentString.append(String.valueOf(mark) + " ");
        }
        studentString.append(System.lineSeparator());
        return studentString.toString();
    }


}
